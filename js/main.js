// CRUD ~ update, delete, create
const DSSV = "DSSV";
var dssv = [];
// Láy dữ liệu lên từ localStorage
var dataJson = localStorage.getItem("DSSV");
if (dataJson) {
  // truthy falsy
  var dataRaw = JSON.parse(dataJson);
  // convert data từ local (object lấy từ  local sẽ mất method  nên method thinhDTB ko còn => gây lỗi)
  // Cách 1:
  // var result = [];
  // for (var index = 0; index < dataRaw.length; index++) {
  //   var currentData = dataRaw[index];
  //   var sv = new SinhVien(
  //     currentData.ma,
  //     currentData.ten,
  //     currentData.email,
  //     currentData.matKhau,
  //     currentData.diemLy,
  //     currentData.diemToan,
  //     currentData.diemHoa
  //   );
  //   result.push(sv);
  // }

  // Cách 2: map
  dssv = dataRaw.map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.diemLy,
      item.diemToan,
      item.diemHoa
    );
  });
  renderDssv(dssv);
}
function saveLocalStorage() {
  var dssvJson = JSON.stringify(dssv);
  localStorage.setItem(DSSV, dssvJson);
}

function themSv() {
  var newSv = layThongTinTuForm();

  var isValid = true;

  // isValid =
  //   kiemTraRong(newSv.ma, "spanMaSV") &
  //   kiemTraRong(newSv.ten, "spanTenSV") &
  //   kiemTraRong(newSv.email, "spanEmailSV") &
  //   kiemTraRong(newSv.matKhau, "spanMatKhau") &
  //   kiemTraRong(newSv.diemToan, "spanToan") &
  //   kiemTraRong(newSv.diemHoa, "spanHoa") &
  //   kiemTraRong(newSv.diemLy, "spanLy");
  // isValid = isValid && kiemTraMaSV(newSv.ma, dssv, "spanMaSV");

  // Kiểm tra mã
  isValid =
    isValid & kiemTraRong(newSv.ma, "spanMaSV") &&
    kiemTraMaSV(newSv.ma, dssv, "spanMaSV");
  // Kiểm tra tên
  isValid = isValid & kiemTraRong(newSv.ten, "spanTenSV");
  // Kiểm tra email
  isValid &=
    kiemTraRong(newSv.email, "spanEmailSV") &&
    kiemTraEmail(newSv.email, "spanEmailSV");
  isValid &= kiemTraRong(newSv.matKhau, "spanMatKhau");
  isValid &= kiemTraRong(newSv.diemToan, "spanToan");
  isValid &= kiemTraRong(newSv.diemHoa, "spanHoa");
  isValid &= kiemTraRong(newSv.diemLy, "spanLy");
  // if(!isValid) return;
  // isValid && dssv.push(newSv);

  if (isValid) {
    dssv.push(newSv);
    saveLocalStorage();
    renderDssv(dssv);
    resetForm();
  }
}
function xoaSv(idSv) {
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSv;
  });

  if (index == -1) return;
  dssv.splice(index, 1);
  console.log("dssv: ", dssv.length);
  renderDssv(dssv);
}
function suaSv(idSv) {
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) return;

  var sv = dssv[index];

  showThongTinLenForm(sv);
  document.getElementById("txtMaSV").disabled = true;
}
function capNhat() {
  var svEdit = layThongTinTuForm();

  var index = dssv.findIndex(function (sv) {
    return sv.ma == svEdit.ma;
  });
  if (index == -1) return;

  dssv[index] = svEdit;
  saveLocalStorage();
  renderDssv(dssv);
  resetForm();
  document.getElementById("txtMaSV").disabled = false;
}
