function layThongTinTuForm() {
  // trim(): bỏ khoảng cách
  var maSv = document.getElementById("txtMaSV").value.trim();
  var tenSv = document.getElementById("txtTenSV").value.trim();
  var email = document.getElementById("txtEmail").value.trim();
  var matKhau = document.getElementById("txtPass").value.trim();
  var diemLy = document.getElementById("txtDiemLy").value.trim();
  var diemHoa = document.getElementById("txtDiemHoa").value.trim();
  var diemToan = document.getElementById("txtDiemToan").value.trim();

  // Tạo object từ thông tin lấy từ form
  var sv = new SinhVien(maSv, tenSv, email, matKhau, diemLy, diemHoa, diemToan);
  return sv;
}
function showThongTinLenForm(SinhVien) {
  document.getElementById("txtMaSV").value = SinhVien.ma;
  document.getElementById("txtTenSV").value = SinhVien.ten;
  document.getElementById("txtEmail").value = SinhVien.email;
  document.getElementById("txtPass").value = SinhVien.matKhau;
  document.getElementById("txtDiemLy").value = SinhVien.diemLy;
  document.getElementById("txtDiemHoa").value = SinhVien.diemHoa;
  document.getElementById("txtDiemToan").value = SinhVien.diemToan;
}
// renderDssv(): Hiển thị dữ liệu dssv
function renderDssv(list) {
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var currentSv = list[i];
    var contentTr = ` <tr>
      <td>${currentSv.ma}</td>
      <td>${currentSv.ten}</td>
      <td>${currentSv.email}</td>
      <td>${currentSv.tinhDTB()}</td>
      <td>
      <button onclick="xoaSv('${
        currentSv.ma
      }')" class="btn btn-danger">Xóa</button>
      <button onclick="suaSv('${
        currentSv.ma
      }')" class="btn btn-primary">Sửa</button>
      </td>
      </tr> `;
    // contentHTML = contentHTML + contentTr;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function resetForm() {
  document.getElementById("formQLSV").reset();
}
