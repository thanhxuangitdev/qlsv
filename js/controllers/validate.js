function kiemTraRong(value, idError) {
  if (value.length == 0) {
    document.getElementById(idError).innerText =
      "Trường này không được để trống";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}
function kiemTraMaSV(idSv, listSv, idError) {
  var index = listSv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Mã này đã tồn tại";
    return false;
  }
}
function kiemTraEmail() {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = re.test(value);
  if (!isEmail) {
    document.getElementById(idError).innerText = "Email không hợp lệ";
    return true;
  } else {
    document.getElementById(idError).innerText = "";
    return false;
  }
}
